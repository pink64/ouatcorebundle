<?php

namespace Ouat\CoreBundle\Command;

use Ouat\CoreBundle\Manager\UniversalDocumentManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestDocumentLoaderCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('ouat:test_document_loader')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $rec = $em->getRepository('AppBundle:Organisation\Agence')->find(1);
        $uid = UniversalDocumentManager::generateDocumentID($rec);

        echo strlen($uid);
        echo strlen(get_class($rec));

        $output->writeln("Document UID : ".$uid);

        $mgd = $this->getContainer()->get('ouat.document_loader');
        $o = $mgd->getDocument($uid);

        $output->writeln("Document : ".get_class($o));
    }
}
