<?php

namespace Ouat\CoreBundle\Controller;

use Ouat\UIBundle\Screen\ScreenConfig;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class BaseController extends Controller
{
    

    protected $vars = array();

    /**
     * Fixe la valeur d'une variable du controleur
     *
     * @param $name
     * @param $value
     *
     */
    protected function setVar($name, $value)
    {
        $this->vars[$name] = $value;
        $this->{$name} = $value;
    }

   

    /**
     * Retourne un tableau contenant l'ensemble des variables
     *
     * @param array $vars
     * @return array
     */
    protected function returnVars($vars = array())
    {
        if ($vars == NULL) $vars = array();
        $vars['_screen'] = $this->getScreenConfig();
        return array_merge($this->vars, $vars);
    }


    protected function returnForm($form,$form_name = 'form',$vars = array()) {
        if ($form_name == NULL) $form_name = 'form' ;
        $this->setVar($form_name,$form->createView());
        return $this->returnVars($vars);
    }

    protected function getArgs($vars = array())
    {
        return array_merge(isset($this->vars['args']) ? $this->vars['args'] : array(), $vars);
    }


    protected function setArg($arg_name, $arg_value, $set_as_var = true)
    {

        if ($set_as_var)
            $this->setVar($arg_name,$arg_value);

        if (is_object($arg_value)) {
            $arg_value = $arg_value->getId();
            if (!$arg_value) $arg_value = 'new' ;
        }




        return $this->vars['args'][$arg_name] = $arg_value;
    }


    protected function redirectPage($route, $args = array())
    {
        return $this->redirect($this->generateUrl($route, $this->getArgs($args)));
    }

    protected function entityFormAction($form_name, $document, $success_route_name, $template_name = NULL)
    {

        $request = $this->getRequest();

        $form = $this->createForm($form_name, $document);

        $em = $this->getDoctrine()->getManager();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $em->flush();

            return $this->redirect($this->generateUrl($success_route_name, $this->getArgs()));
        }

        $template_vars = $this->returnVars(array(
                'form' => $form->createView(),
                'cancel_form_url' => $this->generateUrl($success_route_name, $this->getArgs()))
        );

        if ($template_name)
            return $this->render($template_name, $template_vars);

        return $template_vars;

    }

//    public function persist($object)
//    {
//        $this->getDoctrine()->getEntityManager()->persist($object);
//    }
//
//    public function flush()
//    {
//        $this->getDoctrine()->getEntityManager()->flush();
//    }

    public function closeDialog()
    {
        return $this->render("OuatUIBundle:tools:close_modal.html.twig");
    }

    public function closeForm() {
        return $this->render("OuatUIBundle:window:close_form.html.twig");
    }

    public function returnFile($filePath,$filename,$mime) {
        $map_mimes = array(
            'xlsx' => 'application/vnd.ms-excel',
            'pdf' => 'application/pdf',
            'zip' => 'application/zip'
        );

        if (isset($map_mimes[$mime])) $mime = $map_mimes[$mime];

        $content = file_get_contents($filePath);

        $response = new Response();
        $response->headers->set('Content-type', $mime);
        $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', basename($filename)));
        $response->headers->set('Content-Length', strlen($content));
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->setContent($content);

        return $response;

        return $res ;
    }

    public function returnAsFile($filename, $content, $mime)
    {
        $map_mimes = array(
            'xlsx' => 'application/vnd.ms-excel',
            'pdf' => 'application/pdf',
            'zip' => 'application/zip'
        );

        if (isset($map_mimes[$mime])) $mime = $map_mimes[$mime];

        if (gettype($content) != 'string')
            $content = stream_get_contents($content);

        $response = new Response();
        $response->headers->set('Content-type', $mime);
        $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $filename));
        $response->headers->set('Content-Length', strlen($content));
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->setContent($content);

        return $response;
    }

    public function returnAsFileInline($filename, $content, $mime)
    {
        $map_mimes = array(
            'xlsx' => 'application/vnd.ms-excel',
            'pdf' => 'application/pdf',
            'zip' => 'application/zip'
        );

        if (isset($map_mimes[$mime])) $mime = $map_mimes[$mime];

        if (gettype($content) != 'string')
            $content = stream_get_contents($content);

        $response = new Response();
        $response->headers->set('Content-type', $mime);
//        $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $filename));
        $response->headers->set('Content-Length', strlen($content));
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->setContent($content);

        return $response;
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    public function checkGranted($grantName,$object) {

        if (false === $this->isGranted($grantName, $object)) {
            throw new AccessDeniedException();
        }
    }

    public function returnJSON($vars,$code = NULL) {
        $response = new Response(json_encode($vars));
        $response->headers->set('Content-Type', 'application/json');

        if ($code)
            $response->setStatusCode($code);

        return $response;
    }

    protected function simpleAction(Request $request,$action_name,$object) {
        $mgr_action = $this->get('app.actions_manager');

        $action = $mgr_action->getAction($action_name);

        if (!$this->isGranted($action->getRole(),$object)) {
            die("Role non autorise : ".$action->getRole());
        }

        $form_action = $action->createFormAction($object);

        $form_action->handleRequest($request);

        if ($form_action->isValid()) {
            $action->run($object,$form_action->getData());
            $this->getEntityManager()->flush();
            return $this->closeForm();
        }


        return $this->returnVars(['record'=>$object,'action'=>$action,'form'=>$form_action->createView()]);
    }

    protected function setupBackUrl(Request $request,$default_route=NULL,$default_route_parameters = array()) {
        $back_url = $request->get('_back');

        if (!$back_url)
            $back_url = $this->generateUrl($default_route,$default_route_parameters);

        $this->setVar('back_url',$back_url );

        return $back_url ;
    }

    protected function setOriginFor(Request $request,$name = NULL) {


        if ($name ) {
            $key_name = 'origin_'.$name ;
        }
        else {
//            var_export($request->attributes->all());
//            die();
            $params = explode('::',$request->attributes->get('_controller'));
            $controller_name = $params[0];

            $session = $this->container->get('session');
            $key_name = 'origin_'.$controller_name ;
        }

        $router = $this->get('router');
        $ref = str_replace("app_dev.php/", "", parse_url($request->headers->get('referer'),PHP_URL_PATH ));

        // infos sur l'origine

        $info_ref = $router->match($ref);
        $ref_controller = $info_ref['_controller'] ;
        $ref_class = explode('::',$ref_controller)[0];

        // infos sur la requete courante

//        $params = explode('::',$request->attributes->get('_controller'));
        $controller_class = get_class($this);

        if ($ref_class != $controller_class) {
            $key_name = 'origin_'.$controller_class ;
            $this->get('session')->set($key_name, $request->headers->get('referer'));
        }

    }

    protected function rememberOriginFor(Request $request)
    {
        $this->setOriginFor($request);
    }

    protected function setupScreen(ScreenConfig $config) {
        $config->setScreenColor('green') ;
    }

    protected $screenConfig ;

    public function getScreenConfig() {
        if (!$this->screenConfig) {
            $this->screenConfig = new ScreenConfig();
            $this->setupScreen($this->screenConfig);
        }
        return $this->screenConfig;
    }

//    /**
//     * @return \Oneup\AclBundle\Security\Acl\Manager\AclManager
//     */
//    public function getAclManager() {
//        return $this->get('oneup_acl.manager');
//    }

    // region CRUD

    protected function crudList($request,$options) {

        $qb = $this->getEm()
            ->createQueryBuilder()
            ->select('item')
            ->from($options['entity'],'item');

        $filters = array();

        if ($request->get('form')) {
            $filters = $request->get('filters');
        }

        $form_filters = $this->createFormBuilder($filters)->getForm();
        $form_filters->handleRequest($request);

        if ($form_filters->isValid()) {
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form_filters, $qb);
            $filters = $form_filters->getData();
            unset($filters['_token']);
        }

        $query = $qb->getQuery();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), 9);
        $pagination->setParam('form', $filters);

        $options['table_builder']->setItems($pagination);

        return $this->render('OuatUIBundle:crud:module_liste.html.twig',$this->returnVars([
            'items' => $pagination,
            'th' => $options['table_builder']
        ]));
    }

    protected function crudCreate($request,$options) {
        $form = $this->createForm($options['form_type'], $options['record']);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getEm()->persist($options['record']);
            $this->getEm()->flush();
            $this->notifyUser_success("Création réussie.");
            return $this->redirectPage($options['success']);
        }

        if ($form->getErrors(true)->count()>0) {
            $this->notifyUser_error("Création non réalisée.");
        }

        return $this->render('OuatUIBundle:crud:modal_create.html.twig',$this->returnForm($form));
    }

    protected function crudEdit($request,$options) {
        $form = $this->createForm($options['form_type'], $options['record']);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getEm()->flush();
            $this->notifyUser_success("Mise à jour réussie.");
            return $this->closeDialog();//redirectPage($options['success']);
        }

        if ($form->getErrors(true)->count()>0) {
            $this->notifyUser_error("Mise à jour non réalisée.");
        }

        return $this->render('OuatUIBundle:crud:modal_edit.html.twig',$this->returnForm($form));
    }

    //endregion

    //region Gestion d'un context spécifique au controlleur

    protected function hasPageRemembered() {
        return $this->getScreenSessionVar('last_page') ? true : false ;
    }

    protected function goPageRemembered() {
        $last_page = $this->getScreenSessionVar('last_page') ;
        return $this->redirectToRoute($last_page[0],$last_page[1] === NULL ? array() : $last_page[1]);
    }

    protected function rememberPage(Request $request) {
        $last_page = array(
            $request->attributes->get('_route'),
            $request->attributes->get('_route_params')
        );

        $this->setScreenSessionVar('last_page',$last_page);

    }

    private function getScreenSessionKey() {
        return get_class($this) ;
    }

    protected function setScreenSessionVar($name,$value) {
        $ssv = $this->get('session')->get($this->getScreenSessionKey(),array());
        $ssv[$name] = $value ;
        $this->get('session')->set($this->getScreenSessionKey(),$ssv);
    }

    protected function getScreenSessionVar($name,$default = NULL) {
        $ssv = $this->get('session')->get($this->getScreenSessionKey(),array());
        if (isset($ssv[$name]))
            return $ssv[$name] ;

        return $default ;
    }

    //endregion

    public function renderPage($vars = array()) {

        return $this->render('OuatUIBundle:screen:page.html.twig',$this->returnVars($vars));
    }

    public function renderForm($form,$vars = array()) {

        return $this->render('OuatUIBundle:screen:page.html.twig',$this->returnForm($form,'form',$vars));
    }

    /**
     * @param $type
     * @return ListBuilder
     */
    protected function createListBuilder($type,$data = NULL) {
        $lb = new $type($this->getContainer());

        $lb->start();
        $lb->getTable()->setItems($data);

        return $lb ;
    }
}