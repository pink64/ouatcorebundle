<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 29/06/2016
 * Time: 16:20
 */


namespace Ouat\CoreBundle\Formatter;

class MoneyFormatter {

    protected $show_if_zero = true ;

    protected $precision = 2 ;

    protected $unit = '€' ;

    public function __construct($options = array()) {
        foreach($options as $ko => $vo)
            $this->{ $ko } = $vo ;
    }

    public function getCellStyle($value) {
        return 'text-align:right;width:120px' ;
    }

    public function formatHTML($value,$options = array()) {
        return $this->format($value);
    }

    protected function format($v)
    {
        if (trim($v) == '' || ($v == 0 && $this->show_if_zero == false))
            return '';

        if (!is_numeric($v)) return "NOT A NUMERIC : $v";

        return number_format($v, $this->precision, '.', ' ') . ' '.$this->unit;

    }
}