<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 15/05/2016
 * Time: 14:13
 */

namespace Ouat\CoreBundle\Manager;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class BaseEntityManager implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function __construct(ContainerInterface $container) {
        $this->setContainer($container);
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer() {
        return $this->container ;
    }


    // region Doctrine shortcut
    public function getQuery($filters = array()) {
        return $this->applyFilters($this->getQueryBuilder(),$filters)->getQuery();
    }

    abstract public function applyFilter(QueryBuilder $qb,$name,$value);

    /**
     * @param $qb QueryBuilder
     * @param $filters
     * @return QueryBuilder
     */
    public function applyFilters($qb,$filters) {

        foreach($filters as $kf => $vf) {
            if ($vf!==null && $vf!=='')
                $res = $this->applyFilter($qb, $kf,$vf);

            if ($res === false)
                throw new \Exception("Filtre inconnu : $kf");

        }

        return $qb ;
    }

    /**
     * @param array
     * @return array|ArrayCollection
     */
    public function search($filters = array()) {
        return $this->applyFilters($this->getQueryBuilder(),$filters)
                    ->getQuery()
                    ->getResult();
    }

    /**
     * @return array|ArrayCollection
     */
    public function findAll() {
        return $this->search(array());
    }

    

    /**
     * @return object
     */
    public function find($id) {
        return $this->search(array('id'=>$id));
    }

    //endregion


}