<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 26/05/2016
 * Time: 09:25
 */

namespace Ouat\CoreBundle\Manager;

use AppBundle\AppBundle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Imagine ;

class ImagePreviewManager implements ContainerAwareInterface
{
    use ContainerAwareTrait;



    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    public function getTempDir($create=false) {
        $dir = $this->container->getParameter('kernel.root_dir').'/../var/preview/';

        if ($create && !file_exists($dir)) {
            mkdir($dir,0700,true);
        }

        return $dir ;
    }


    public function getPreviewFilePath($realPath) {
        return $this->getTempDir().basename($realPath);
    }

    public function deletePreview($realPath) {
        if (file_exists($target = $this->getPreviewFilePath($realPath)))
            unlink($target);
    }

    public function getPreview($realPath) {
        $target = $this->getPreviewFilePath($realPath);


        if (!file_exists($target)) {
            $imagine = new \Imagine\Gd\Imagine();
            $img =  $imagine->open($realPath);


            $old_x          =   $img->getSize()->getWidth();
            $old_y          =   $img->getSize()->getHeight();

            $newWidth = 300 ;
            $newHeight = 300 ;

            if($old_x > $old_y)
            {
                $thumb_w    =   $newWidth;
                $thumb_h    =   $old_y/$old_x*$newWidth;
            }

            if($old_x < $old_y)
            {
                $thumb_w    =   $old_x/$old_y*$newHeight;
                $thumb_h    =   $newHeight;
            }

            if($old_x == $old_y)
            {
                $thumb_w    =   $newWidth;
                $thumb_h    =   $newHeight;
            }

            $imagine->open($realPath)
                ->resize(new Imagine\Image\Box($thumb_w,$thumb_h))
                ->save($target);
        }

        return $target ;
    }
}