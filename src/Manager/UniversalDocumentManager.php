<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 26/05/2016
 * Time: 09:25
 */

namespace Ouat\CoreBundle\Manager;

use AppBundle\AppBundle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UniversalDocumentManager implements ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    protected $container ;


    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }



    public static function generateDocumentID($object) {
        return gzcompress(get_class($object).':'.$object->getId());
    }

    public function getDocument($udid) {

        list($class,$id) = explode(':',gzuncompress($udid));

        return $this->container->get('doctrine.orm.entity_manager')->getRepository($class)->find($id);
    }
}