<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 26/05/2016
 * Time: 09:25
 */

namespace Ouat\CoreBundle\Manager;

use AppBundle\AppBundle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UpFileManager implements ContainerAwareInterface
{
    use ContainerAwareTrait;



    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    public function getTempDir($create=false) {
        $dir = $this->container->getParameter('kernel.root_dir').'/../var/uploads/';

        if ($create && !file_exists($dir)) {
            mkdir($dir,0700,true);
        }

        return $dir ;
    }

    public function createCopy($sourceFile) {
        $path_parts = pathinfo($sourceFile);
        $fileID = md5(uniqid().'.'.basename($sourceFile)).'.'.$path_parts['extension'];
        $target = $this->getFilePath($fileID);
        copy($sourceFile,$target);
        return $fileID ;
    }

    /**
     * @param UploadedFile $file
     * @return string|fileID
     */
    public function saveFile(UploadedFile $file) {
        $fileID = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->getTempDir(true),$fileID);

        return $fileID ;
    }

    protected function cleanID($fileID) {
        $repl = array('/'=>'');
        return str_replace(array_keys($repl),$repl ,$fileID );
    }

    /**
     * @param $fileID string
     */
    public function getFilePath($fileID) {
        return $this->getTempDir().$this->cleanID($fileID) ;
    }

    public function deleteFile($fileID) {
        $filename = $this->getFilePath($fileID);
        if (file_exists($filename))
            unlink($filename);
    }
}