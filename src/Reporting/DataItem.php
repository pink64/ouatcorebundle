<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 21/06/2016
 * Time: 13:57
 */


namespace Ouat\CoreBundle\Reporting;

class DataItem extends Item
{
    /**
     * @var string
     */
    protected $label ;

    /**
     * @var mixed
     */
    protected $data ;

    /**
     * @var $datas
     */
    protected $datas = array() ;

    /**
     * @var float
     */
    protected $coutUnitaire;

    /**
     * @var float
     */
    protected $qtt;

    protected function fillPath(\ArrayObject $path_elements) {
        if ($this->getParent()) {
            $this->getParent()->fillPath($path_elements);
            $path_elements->append('items');

        }


        $path_elements->append($this->itemKey);

//        $path_elements->append($this->getItemKey());
    }

    public function getTotalHT() {
        return $this->getQtt() * $this->getCoutUnitaire();
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getDatas()
    {
        return $this->datas;
    }

    /**
     * @param mixed $datas
     */
    public function setDatas($datas)
    {
        $this->datas = $datas;
    }

    /**
     * @return float
     */
    public function getCoutUnitaire()
    {
        return $this->coutUnitaire;
    }

    /**
     * @param float $coutUnitaire
     */
    public function setCoutUnitaire($coutUnitaire)
    {
        $this->coutUnitaire = $coutUnitaire;
    }

    /**
     * @return float
     */
    public function getQtt()
    {
        return $this->qtt;
    }

    public function addQtt($qtt) {
        $this->qtt += $qtt ;
    }

    /**
     * @param float $qtt
     */
    public function setQtt($qtt)
    {
        $this->qtt = $qtt;
    }



}