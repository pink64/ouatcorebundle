<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 21/06/2016
 * Time: 13:54
 */

namespace Ouat\CoreBundle\Reporting;

class Item {

    /**
     * @var Item
     */
    protected $parent ;

    protected $itemKey ;

    public function __construct($parent = NULL)
    {
        $this->parent = $parent ;

        $this->configure();
    }


    protected function fillPath(\ArrayObject $path_elements) {
        if ($this->getParent())
            $this->getParent()->fillPath($path_elements);

        $path_elements->append($this->getItemKey());
    }

    public function getPath() {
        $path_elements = new \ArrayObject();
        $this->fillPath($path_elements);
        return implode('.',$path_elements->getArrayCopy());

    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getItemKey()
    {
        return $this->itemKey;
    }

    /**
     * @param mixed $itemKey
     */
    public function setItemKey($itemKey)
    {
        $this->itemKey = $itemKey;
    }

    public function getByPath($path) {
        $path_elements = explode('.',$path);
        if (count($path_elements)==1)
            return $this ;

        return '?'.$path;
    }



    protected function configure() {

    }


}