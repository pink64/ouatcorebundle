<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 21/06/2016
 * Time: 10:31
 */

namespace Ouat\CoreBundle\Reporting;

use Doctrine\Common\Collections\ArrayCollection;

class Reporting extends Section
{
    const PREVU = 'prevu' ;
    const REEL = 'reel' ;

    protected function fillPath(\ArrayObject $path_elements) {
//        $path_elements->append('sections');
        $path_elements->append('reporting');
    }

    public function getByPath($path) {
        $path_elements = explode('.',$path);
        switch($path_elements[0]) {
            case 'reporting' :
                $path_elements = array_slice($path_elements,1);
                return $this->getByPath(implode('.',$path_elements));

            default:
                return parent::getByPath($path);

        }
    }
}