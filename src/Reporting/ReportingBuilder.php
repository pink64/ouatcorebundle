<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 30/05/2016
 * Time: 16:20
 */
namespace Ouat\CoreBundle\Reporting;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReportingBuilder {

    protected $templateFile ;

    /**
     * @var ContainerInterface
     */
    protected $container;

    protected $dataSheetName ;

    public function __construct(ContainerInterface $container) {
        $this->setContainer($container);
    }

    



    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }



    /**
     * @return mixed
     */
    public function getTemplateFile()
    {
        return $this->templateFile;
    }

    /**
     * @param mixed $templateFile
     */
    public function setTemplateFile($templateFile)
    {
        $this->templateFile = $templateFile;
    }

    /**
     * @return mixed
     */
    public function getDataSheetName()
    {
        return $this->dataSheetName;
    }

    /**
     * @param mixed $dataSheetName
     */
    public function setDataSheetName($dataSheetName)
    {
        $this->dataSheetName = $dataSheetName;
    }


    public function generate($data) {
        /**/
        $tmp = $this->container->get('ouat.manager.upfile');
        $fileID = $tmp->createCopy($this->getTemplateFile());
        $filePath = $tmp->getFilePath($fileID);

        //        $workfile =
        $xls = $this->container->get('phpexcel')->createPHPExcelObject($filePath);
        $sheet = $xls->getSheetByName($this->getDataSheetName());

        if (count($data)!=0) {
            $header = array_keys($data[0]);

            $startIndex = 2 ;
            foreach($data as $ir => $row) {
                $ic = 0 ;
                foreach($row as $k => $v) {

                    $cellname = \PHPExcel_Cell::stringFromColumnIndex($ic) . ($ir + $startIndex);
                    $sheet->setCellValue($cellname,$v);
                    $ic++;
                }

            }
        }

        $writer = $this->container->get('phpexcel')->createWriter($xls, 'Excel2007');
        $writer->save($filePath);
        return array('fileID'=>$fileID,'filePath'=>$filePath);
    }

}