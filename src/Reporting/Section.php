<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 21/06/2016
 * Time: 10:30
 */

namespace Ouat\CoreBundle\Reporting;

use Doctrine\Common\Collections\ArrayCollection;

class Section extends Item {

    

    /**
     * @var string
     */
    protected $code ;

    /**
     * @var string
     */
    protected $label ;

    /**
     * @var array|ArrayCollection|Section[]
     */
    protected $sections = array();

    /**
     * @var array|ArrayCollection|DataItem[]
     */
    protected $items = array();

    public function __construct($parent = NULL)
    {
        $this->sections = new ArrayCollection();
        $this->items = new ArrayCollection();

        parent::__construct($parent);
    }

    protected function fillPath(\ArrayObject $path_elements) {
        if ($this->getParent()) {
            $this->getParent()->fillPath($path_elements);
            $path_elements->append('sections');

        }


        $path_elements->append($this->code);

//        $path_elements->append($this->getItemKey());
    }



    public function setSection($name,Section $section) {
        $section->setCode($name);
        $section->setParent($this);
        $this->sections->add($section);

        return $section ;

    }

    public function getSection($name) {
        foreach($this->sections as $section)
            if ($section->getCode() == $name)
                return $section ;



        $section = new Section($this);

        $this->setSection($name,$section);


        return $section ;
    }

    public function getSections() {
        return $this->sections ;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this ;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this ;
    }

    /**
     * @return array|ArrayCollection|DataItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array|ArrayCollection|DataItem[] $items
     */
    public function addItem(DataItem $item)
    {
        $item->setParent($this);
        $this->items->add($item);
    }

    public function getItemByKey($key) {
        foreach($this->getItems() as $item)
            if ($item->getItemKey() == $key)
                return $item ;

        return NULL ;
    }

    public function hasItem($key) {
        return $this->getItemByKey($key) ? true : false ;
    }

    public function createItem($key,$label,$coutUnitaire) {
        $item = new DataItem();
        $item->setItemKey($key);
        $item->setLabel($label);
        $item->setCoutUnitaire($coutUnitaire);



        $this->addItem($item);

        return $item ;
    }

    public function getTotalHT() {
        $total = 0 ;
        foreach($this->getSections() as $section) {
            $total += $section->getTotalHT();
        }

        foreach($this->getItems() as $item) {
            $total += $item->getTotalHT();
        }

        return $total ;
    }


    public function getByPath($path) {
        if (!$path)
            return $this ;


        $path_elements = explode('.',$path);
        switch($path_elements[0]) {
            case 'sections' :
                $section = $this->getSection($path_elements[1]);
                $path_elements = array_slice($path_elements,2);
                return $section->getByPath(implode('.',$path_elements));

            case 'items' :
                $item = $this->getItemByKey($path_elements[1]);

                if ($item == NULL) {
                    return NULL ;
//                    die($path_elements[1]);
                }

                $path_elements = array_slice($path_elements,2);
                return $item->getByPath(implode('.',$path_elements));

        }
    }


}