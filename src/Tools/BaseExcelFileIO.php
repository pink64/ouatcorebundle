<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 26/05/2016
 * Time: 09:49
 */

namespace Ouat\CoreBundle\Tools;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BaseExcelFileIO extends BaseFileIO {
    protected $excel ;
    protected $reader ;

    /**
     * @return \PHPExcel
     */
    protected function getExcel() {
        if (!$this->excel) {
            $this->excel = $this->container->get('phpexcel')->createPHPExcelObject($this->getFilename());
        }
        return $this->excel ;
    }

    protected function getCurrentSheet() {
        return $this->getExcel()->getActiveSheet();
    }

    protected $headers ;

    protected function makeCellName($colIndex,$rowIndex) {
        return \PHPExcel_Cell::stringFromColumnIndex($colIndex);
    }

    protected function getValue($x,$y) {

    }


    protected $headers_row_index = 0 ;

    /**
     * @return int
     */
    public function getHeadersRowIndex()
    {
        return $this->headers_row_index;
    }

    /**
     * @param int $headers_row_index
     */
    public function setHeadersRowIndex($headers_row_index)
    {
        $this->headers_row_index = $headers_row_index;
    }

    public function getDataRowStartIndex() {
        return $this->getHeadersRowIndex()+1;
    }









    protected function getHeaders() {

        if (!$this->headers) {
            $this->readHeaders();
        }

        return $this->headers;
    }

    protected $rows = NULL;

    protected function getRows() {
        if ($this->rows === NULL) {
            $this->rows = $this->getCurrentSheet()->toArray();
        }

        return $this->rows ;
    }

    protected function createLineFromRow($row) {
        $line = array();
        foreach($this->getHeaders() as $kh => $vh) {
            if (isset($row[$kh]))
                $line[$vh] = $row[$kh];
            else
                $line[$vh] = NULL ;
        }

        return $line ;
    }

    protected function getRow($rowIndex) {
        if (!$this->rows)
            $this->getRows();

        return isset($this->rows[$rowIndex]) ? $this->rows[$rowIndex] : NULL ;
    }

    protected function getRowsCount() {
        return count($this->rows);
    }

    protected function readHeaders() {
        $this->headers = $this->getRow($this->getHeadersRowIndex());

    }

    protected function parseMoney($value) {
        $value = str_replace(array('$','€'),array('',''),$value);
        $value = trim($value);
        
        return floatval(str_replace(array(',',' '),array('.',''),$value)) ;
    }

    protected $lines = array();

    public function toLines() {
        $doc = $this->getExcel();

        $sheet = $doc->getActiveSheet();
        $lastRow = $sheet->getHighestRow();

        $rows = $this->getRows();

        $this->lines = array();
        $row_count = $this->getRowsCount();

        for($ir=$this->getDataRowStartIndex();$ir<$row_count;$ir++) {
            $row = $this->getRow($ir);
            $line = $this->createLineFromRow($row);
            $line = $this->transformRawRow($line);
            if ($line)
                $this->lines[] = $line ;
        }

        return $this->lines;
    }

    public function transformRawRow($row) {
        return $row ;
    }
}