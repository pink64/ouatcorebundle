<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 27/06/2016
 * Time: 13:51
 */

namespace Ouat\CoreBundle\Tools;

use Doctrine\Common\Annotations\AnnotationReader;
use Sensio\Bundle\FrameworkExtraBundle\Security\ExpressionLanguage;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class RouteSecurityChecker {

    private $tokenStorage;
    private $authChecker;
    private $language;
    private $trustResolver;
    private $roleHierarchy;
    private $annotationReader;
    private $router ;

    public function __construct(SecurityContextInterface $securityContext = null, ExpressionLanguage $language = null, AuthenticationTrustResolverInterface $trustResolver = null, RoleHierarchyInterface $roleHierarchy = null, TokenStorageInterface $tokenStorage = null, AuthorizationCheckerInterface $authChecker = null,AnnotationReader $annotationReader,RouterInterface $router)
    {
        $this->tokenStorage = $tokenStorage ?: $securityContext;
        $this->authChecker = $authChecker ?: $securityContext;
        $this->language = $language;
        $this->trustResolver = $trustResolver;
        $this->roleHierarchy = $roleHierarchy;
        $this->annotationReader = $annotationReader ;
        $this->router = $router ;
    }

    public function checkUrl($url,$route_parameters) {
        foreach($route_parameters as $k => $v) {
            error_log("$k > ".gettype($v));
        }
        $controller = $this->router->match($url);
        error_log("controller");
        $controller = $controller['_controller'];

        list($class, $method) = explode('::', $controller, 2);

        $rc = new \ReflectionClass($class);
        $rm = $rc->getMethod($method);



        return $this->check($rm,$route_parameters);
    }

    public function checkRoute($route_name,$route_parameters) {

        $params=array();
        foreach($route_parameters as $k=>$v) {
            if (is_object($v))
                $params[$k] = $v->getId();
            else
                $params[$k] = $v ;
        }

        $url = $this->router->generate($route_name,$params);

        return $this->checkUrl($url,$route_parameters);

        $controller = $this->router->match($url);
        $controller = $controller['_controller'];

        list($class, $method) = explode('::', $controller, 2);

        $rc = new \ReflectionClass($class);
        $rm = $rc->getMethod($method);



        return $this->check($rm,$route_parameters);
    }

    public function check(\ReflectionMethod $rm,$params)
    {
        $configuration = $this->annotationReader->getMethodAnnotation($rm,'Sensio\Bundle\FrameworkExtraBundle\Configuration\Security');

        if (!$configuration) {
            return false ;
        }
        
//        $request = $event->getRequest();
//        if (!$configuration = $request->attributes->get('_security')) {
//            return;
//        }

        if (null === $this->tokenStorage || null === $this->trustResolver) {
            return false ;
            throw new \LogicException('To use the @Security tag, you need to install the Symfony Security bundle.');
        }

        if (null === $this->tokenStorage->getToken()) {
            return false ;
            throw new \LogicException('To use the @Security tag, your controller needs to be behind a firewall.');
        }

        if (null === $this->language) {
            return false ;
            throw new \LogicException('To use the @Security tag, you need to use the Security component 2.4 or newer and to install the ExpressionLanguage component.');
        }

        if (!$this->language->evaluate($configuration->getExpression(), array_merge($this->getVariables(NULL),$params))) {
            return false ;
//            throw new AccessDeniedException(sprintf('Expression "%s" denied access.', $configuration->getExpression()));
        }

        return true ;
    }

    // code should be sync with Symfony\Component\Security\Core\Authorization\Voter\ExpressionVoter
    private function getVariables($request = NULL)
    {
        $token = $this->tokenStorage->getToken();

        if (null !== $this->roleHierarchy) {
            $roles = $this->roleHierarchy->getReachableRoles($token->getRoles());
        } else {
            $roles = $token->getRoles();
        }

        $variables = array(
            'token' => $token,
            'user' => $token->getUser(),
            'object' => $request,
            'request' => $request,
            'roles' => array_map(function ($role) { return $role->getRole(); }, $roles),
            'trust_resolver' => $this->trustResolver,
            // needed for the is_granted expression function
            'auth_checker' => $this->authChecker,
        );

        // controller variables should also be accessible
        return
            $request ?
            array_merge($request->attributes->all(), $variables) :
            $variables;
    }
}