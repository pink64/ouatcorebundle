<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 08/06/2016
 * Time: 11:11
 */

namespace Ouat\CoreBundle\Workflow;

use Symfony\Component\DependencyInjection\ContainerInterface;

class WorkflowProcess {
    /**
     * @var ContainerInterface
     */
    protected $container;

    const workflow_name = 'a_definir' ;

    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }


    protected $process_handler ;

    protected function getWorkflowName() {
        $c = get_called_class();
        return $c::workflow_name ;
    }

    /**
     * @return \Lexik\Bundle\WorkflowBundle\Handler\ProcessHandler
     */
    public function getProcessHandler() {
        if (!$this->process_handler) {
            $this->process_handler = $this->getContainer()->get('lexik_workflow.handler.'.$this->getWorkflowName());
        }
        return $this->process_handler;
    }

    /**
     * @param  $record
     * @return Lexik\Bundle\WorkflowBundle\Entity\ModelState
     */
    public function startProcess($record) {
        $processHandler = $this->getProcessHandler();
        $model = $this->getModel($record);

        $modelState = $processHandler->start($model);

        return $modelState ;
    }

    /**
     * @return \Lexik\Bundle\WorkflowBundle\Flow\Process
     */
    public function getProcess() {
        $pa = $this->container->get('lexik_workflow.process_aggregator');
        return $pa->getProcess($this->getWorkflowName());
    }

    public function getNextSteps($record) {
        $res = array();

        $model = $this->getModel($record);
        $currentState = $this->getProcessHandler()->getCurrentState($model);
        $process = $this->getProcess();

        if ($currentState) {

            $currentStep = $process->getStep($currentState->getStepName());

            foreach($currentStep->getNextStates() as $kn => $nextState) {
                $step_info = array('step_name'=>$kn,'label'=>str_replace('_',' ',$kn));

                $res[$kn] = $step_info ;
            }
        } else {
            // lancement ?
            $kn = '!start';
            $step_info = array('step_name'=>$kn,'label'=>"Transmettre pour validation");

            $res[$kn] = $step_info ;
        }



        return $res ;
    }

    /**
     * @param $record
     * @return \Lexik\Bundle\WorkflowBundle\Entity\ModelState
     */
    public function getCurrentState($record) {
        $model = $this->getModel($record);
        return $this->getProcessHandler()->getCurrentState($model);
    }

    public function reachNextStep($record,$step_name) {


        switch($step_name) {
            case '!start' :
                $modelState =  $this->startProcess($record);
                break ;

            default:
                $model = $this->getModel($record);
                $modelState = $this->getProcessHandler()->reachNextState($model,$step_name);

        }


        return $modelState ;
    }

    /**
     * @param $record
     * @return WorkflowTransition
     */
    public function getTransition($record,$next_step) {
        $process = $this->getProcess();

        $model = $this->getModel($record);

        $currentState = $this->getCurrentState($record);
        $transition = new WorkflowTransition();
        $transition->setWorkflow($this);
        
        if ($currentState) {
            $currentStep = $process->getStep($currentState->getStepName());



            $transition->setFromStep($currentStep);
            $transition->setRecord($record);


            $nexts = $currentStep->getNextStates();


            $nextState =$nexts[$next_step];
            $nextStep = $nextState->getTarget();
            $transition->setToStep($nextStep);



        }


        $this->configureTransition($transition);
        return $transition ;
    }
}