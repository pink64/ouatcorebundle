<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 08/06/2016
 * Time: 14:39
 */

namespace Ouat\CoreBundle\Workflow;

use Lexik\Bundle\WorkflowBundle\Entity\ModelState;
use Lexik\Bundle\WorkflowBundle\Flow\Step;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;

class WorkflowTransition {

    /**
     * @var WorkflowProcess
     */
    protected $workflow ;

    /**
     * @var object
     */
    protected $record ;

    /**
     * @return object
     */
    public function getRecord()
    {
        return $this->record;
    }

    /**
     * @param object $record
     */
    public function setRecord($record)
    {
        $this->record = $record;
    }




    /**
     * @return WorkflowProcess
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }

    /**
     * @param WorkflowProcess $workflow
     */
    public function setWorkflow($workflow)
    {
        $this->workflow = $workflow;
    }

    /**
     * @var Step
     */
    protected $fromStep ;

    /**
     * @var Step
     */
    protected $toStep ;

    /**
     * @return Step
     */
    public function getFromStep()
    {
        return $this->fromStep;
    }

    /**
     * @param Step $fromStep
     */
    public function setFromStep($fromStep)
    {
        $this->fromStep = $fromStep;
    }

    /**
     * @return Step
     */
    public function getToStep()
    {
        return $this->toStep;
    }

    /**
     * @param Step $toStep
     */
    public function setToStep($toStep)
    {
        $this->toStep = $toStep;
    }

    public function getCode() {
        $names = [] ;
        if ($this->getFromStep())
            $names[]= $this->getFromStep() ;

        if ($this->getToStep())
            $names[]= $this->getToStep() ;

        return implode('_TO_',$names);
    }


    /**
     * @var FormBuilderInterface
     */
    protected $formBuilder ;

    /**
     * @return FormBuilderInterface
     */
    public function getFormBuilder()
    {
        if (!$this->formBuilder) {
            $this->formBuilder = $this->getWorkflow()->getContainer()->get('form.factory')->createBuilder()
                ->add('confirmer',CheckboxType::class,array('required'=>true,'label'=>"Veuillez cocher la case pour confirmer"))
            ;
        }
        return $this->formBuilder;
    }

    /**
     * @param FormBuilderInterface $formBuilder
     */
    public function setFormBuilder($formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }

    public function getForm() {
        if (!$this->getFormBuilder())
            return NULL ;

        return $this->formBuilder->getForm() ;
    }

}