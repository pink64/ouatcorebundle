OuatCoreBundle
==============

Installation
============

You can install this bundle using composer

```
composer require ouat/core-bundle
```

or add the package to your composer.json file directly.

After you have installed the package, you just need to add the bundle to your AppKernel.php file:

```
// in AppKernel::registerBundles()
$bundles = array(
    // ...
    new JMS\SerializerBundle\JMSSerializerBundle(),
    // ...
);
```

Content
=======

"liip/imagine-bundle": "^1.5",
"stof/doctrine-extensions-bundle": "^1.2",
"friendsofsymfony/user-bundle": "~2.0@dev",
"friendsofsymfony/jsrouting-bundle": "^1.6",
"symfony/assetic-bundle": "^2.8",
"jms/serializer-bundle": "^1.1"